import { BaseLanguageClient } from 'vscode-languageclient';
import * as vscode from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { CompletionStream } from './completion_stream';
import { createStreamIterator } from './create_stream_iterator';

jest.mock('./create_stream_iterator', () => ({
  createStreamIterator: jest.fn(),
}));

const mockCleanupFn = jest.fn();

describe('CompletionStream', () => {
  it('binds the CompletionStream instance to the cleanup function ', () => {
    const stream = new CompletionStream(
      createFakePartial<BaseLanguageClient>({}),
      createFakePartial<vscode.TextDocument>({}),
      createFakePartial<vscode.Position>({}),
      'stream-1',
      mockCleanupFn,
    );

    const cleanupFn = (createStreamIterator as jest.Mock).mock.calls[0][3];
    cleanupFn();

    expect(mockCleanupFn).toHaveBeenCalledWith(stream);
  });
});
